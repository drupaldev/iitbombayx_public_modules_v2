Drupal Frontend for edx  (IITBombayx Version). For drupal 8.9.0/9.0.1/9.2.1


**Introduction**

Drupal Fontend for openedx setup is developed at IITBombay to integrate user  interface for openedx platform. 

The information mentioned within this manual includes installation instruction of drupal based IITBombayX theme and modules, instructions to customize frontend appearance  and configuration required for integration with Openedx Platform.


**How to Install**

    1. Download provided installation script
      
	wget http://gitlab.cse.iitb.ac.in/drupaldev/iitbombayx_public_modules_v2/-/raw/master/install.sh
      
    2. Make script executable 
             $ chmod +x install.sh
      
    3. Execute script 
	         $ sudo ./install.sh --sitename=<YOUR_SITE_NAME>
      e.g    $ sudo ./install.sh --sitename=demoX
      
    4. Load Digital Assets directory 
      
       While installation of drupal site, script will ask for digital assets directory path, 
      
       - Enter correct path of git’s digital directory  ‘Download source code (tar.gz)’ Path
       - Press enter

       NOTE: To get your digital directory path 
             - Go to your git repository, where your digital assests placed
             - Click on download arrow
             - Right click on  ‘tar.gz’
             - Copy link address

    5. Go to browser and type localhost or Your-IPaddress.
       e.g  http://localhost/


**For detailed Pre-requisite, Installation and Configuration**, Please refer to [Drupal Front End Documentation.pdf](Drupal Front End Documentation .pdf )