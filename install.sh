#!/bin/bash

#
# Script to Install IITBombayX Front End [ MySQL db + PHP + Drush + Drupal ]
#
# @author   IITBombay Drupal Team
# @version  0.1.1

################################################################################
# CORE FUNCTIONS - Do not edit
################################################################################
#
# VARIABLES

_bold=$(tput bold)
_underline=$(tput sgr 0 1)
_reset=$(tput sgr0)

_purple=$(tput setaf 171)
_red=$(tput setaf 1)
_green=$(tput setaf 76)
_tan=$(tput setaf 3)
_blue=$(tput setaf 38)
_zero=0

APACHE_INSTALLED=false
MYSQL_INSTALLED=false
MYSQL_DB_CREATED=false
COMPOSER_INSTALLED=false
DRUSH_INSTALLED=false
PHP_INSTALLED=false
DRUPAL_INSTALLED=false
IITBOMBAYX_PROFILE=false

DRUPALVERSION='9.2.1'


#
# HEADERS & LOGGING
#
function _debug()
{
    [ "$DEBUG" -eq 1 ] && $@
}

function _header()
{
    printf "\n${_bold}${_purple}==========  %s  ==========${_reset}\n" "$@"
}

function _arrow()
{
    printf "➜ $@\n"
}

function _success()
{
    printf "${_green}✔ %s${_reset}\n" "$@"
}


function _process()
{
    printf "${_blue}➜ %s${_reset}\n" "$@"
}


function _error() {
    printf "${_red}✖ %s${_reset}\n" "$@"
}

function _warning()
{
    printf "${_tan}➜ %s${_reset}\n" "$@"
}

function _underline()
{
    printf "${_underline}${_bold}%s${_reset}\n" "$@"
}

function _bold()
{
    printf "${_bold}%s${_reset}\n" "$@"
}

function _note()
{
    printf "${_underline}${_bold}${_blue}Note:${_reset}  ${_blue}%s${_reset}\n" "$@"
}

function _die()
{
    _error "$@"
    exit 1
}

function _safeExit()
{
    exit 0
}

#
# UTILITY HELPER
#
function _seekConfirmation()
{
  printf "\n${_bold}$@${_reset}"
  read -p " (y/n) " -n 1
  printf "\n"
}

# Test whether the result of an 'ask' is a confirmation
function _isConfirmed()
{
    if [[ "$REPLY" =~ ^[Yy]$ ]]; then
        return 0
    fi
    return 1
}


function _typeExists()
{
    if [ $(type -P $1) ]; then
        return 0
    fi
    return 1
}

function _isOs()
{
    if [[ "${OSTYPE}" == $1* ]]; then
      return 0
    fi
    return 1
}

function _checkRootUser()
{
    #if [ "$(id -u)" != "0" ]; then
    if [ "$(whoami)" != 'root' ]; then
        echo "You have no permission to run $0 as non-root user. Use sudo"
        exit 1;
    fi

}

function _checkInternet()
{
   case "$(curl -s --max-time 2 -I http://google.com | sed 's/^[^ ]*  *\([0-9]\).*/\1/; 1q')" in
  [23]) echo "HTTP connectivity is up"  ;;
  5) echo "The web proxy won't let us through"
     exit 1;;
  *) echo "The network is down or very slow"
     exit 1 ;;
   esac
}

function _printPoweredBy()
{
cat <<"EOF"

Powered By:
===========================================================================
 _____ _____ _______ ____                  _                
 |_   _|_   _|__   __|  _ \                | |               
   | |   | |    | |  | |_) | ___  _ __ ___ | |__   __ _ _   _ 
   | |   | |    | |  |  _ < / _ \| '_ ` _ \| '_ \ / _` | | | |
  _| |_ _| |_   | |  | |_) | (_) | | | | | | |_) | (_| | |_| |
 |_____|_____|  |_|  |____/ \___/|_| |_| |_|_.__/ \__,_|\__, |
                                                         __/ |      
                                                        |___/       
============================================================================
EOF
}

################################################################################
# SCRIPT FUNCTIONS
################################################################################
function generatePassword()
{
    echo "$(openssl rand -base64 12)"
}

function generateSimplePassword()
{
   cat /dev/urandom | tr -cd '[:alnum:]' | head -c 12
}

function _printUsage()
{
    echo -n "$(basename $0) [OPTION]...

Install IITBombayX Front End [ MySQL db + PHP + Drush + Drupal ]
Version $VERSION

    Options:       
        -s, --sitename     Drupal Site name        
        -h, --help         Display this help and exit
        -v, --version      Output version information and exit

    Examples:
        ./$(basename $0) --help
        ./$(basename $0) --sitename=test

"
    _printPoweredBy
    exit 1
}

function processArgs()
{
    # Parse Arguments
    for arg in "$@"
    do
        case $arg in           
            -s=*|--sitename=*)
                SITE_NAME_RAW="${arg#*=}"
                SITE_NAME_RAWX="$(echo -e "${SITE_NAME_RAW}" | tr -d '[:space:]')"
                SITE_NAME="$(echo -e "${SITE_NAME_RAWX}" | tr '[:upper:]' '[:lower:]')"             
                DB_NAME="db_${SITE_NAME}"
                DB_USER="${SITE_NAME}"
            ;;
            --debug)
                DEBUG=1
            ;;
            -h|--help)
                _printUsage
            ;;
            *)
                _printUsage
            ;;
        esac
    done 
        [[ -z $SITE_NAME ]] && _error "Site name cannot be empty." && exit 1 
}

function createMysqlDbUser()
{
  if [ "$MYSQL_INSTALLED" = true ] ; then
    SQL0="DROP DATABASE IF EXISTS ${DB_NAME};"
    SQL1="CREATE DATABASE IF NOT EXISTS ${DB_NAME};"    
    SQL2="CREATE USER '${DB_USER}'@'localhost' IDENTIFIED BY '${DB_PASS}';"
    SQL3="GRANT ALL PRIVILEGES ON ${DB_NAME}.* TO '${DB_USER}'@'localhost';"
    SQL4="FLUSH PRIVILEGES;"
    SQL5="DROP USER '${DB_USER}'@'localhost';"
    BIN_MYSQL=$(which mysql)

#echo SQL2

     if [ -f /root/.my.cnf ]; then
      
          $BIN_MYSQL -e "${SQL0}${SQL1}"
         USER_EXIST=$($BIN_MYSQL -s -u root -p${DB_ROOT_PASS} -e "select count(user) from mysql.user where user = '${DB_USER}'")

         
         MYSQL_DB_CREATED=true 
         if [ ! $USER_EXIST -eq $_zero ]; then
          $BIN_MYSQL -e "${SQL5}"
         fi
          $BIN_MYSQL -e "${SQL4}${SQL2}${SQL3}${SQL4}"                 
       
    else

       if [ -z "$DB_ROOT_PASS" ]; then
        _arrow "Please enter root user MySQL password!"
          read rootPassword   
          DB_ROOT_PASS=$rootPassword
         fi        
        
         $BIN_MYSQL -h $DB_HOST -u root -p${DB_ROOT_PASS} -e "${SQL0}${SQL1}" || _die "Cant create database"
         MYSQL_DB_CREATED=true 
         USER_EXIST=$($BIN_MYSQL -s -u root -p${DB_ROOT_PASS} -e "select count(user) from mysql.user where user = '${DB_USER}'")

          
         if [ ! $USER_EXIST -eq $_zero ]; then
          $BIN_MYSQL -h $DB_HOST -u root -p${DB_ROOT_PASS} -e "${SQL5}"
         fi
          $BIN_MYSQL -h $DB_HOST -u root -p${DB_ROOT_PASS} -e "${SQL4}${SQL2}${SQL3}${SQL4}"|| _die "Cant create database user"
     fi

     
    _success "Mysql Database and User created."

    fi
}

function printSuccessMessage()
{
  if [ "$IITBOMBAYX_PROFILE" = true ]; then

cat > /var/www/html/$SITE_NAME/site_credentials.txt << EOF 
     IITBombayX Front End [ MySQL db + PHP + Drush + Drupal ]
     ---------------------------------------------------------------------------------------------- 
     Please note following details for future reference. Also saved as site_credentials.txt file in Drupal root.
     
     Database Host          : ${DB_HOST}
     Database Name          : ${DB_NAME}
     Database User          : ${DB_USER}
     Database Pass          : ${DB_PASS}
     Drupal Sitename        : ${SITE_NAME_RAWX}
     Drupal Admin Username  : ${SITE_USER}
     Drupal Admin Password  : ${SITE_PASS}

     Drupal Frontend Installed Successfully.     
     ------------------------------------------------------------------------------------------------
EOF
cat /var/www/html/$SITE_NAME/site_credentials.txt
else
  _warning "Something went wrong in installation."
fi
}

function installApache()
{
   _process "Installing Apache2........." 
   if dpkg -l | grep -E '^ii' | grep apache2
   then 
   _warning "Apache is already Installed."
   APACHE_INSTALLED=true
   else 
     apt-get install -y apache2 || _die "Error while installing Apache2"
     service apache2 restart
     APACHE_INSTALLED=true
   _success "Done."
   fi
}

function installMysql()
{
    _process "Installing Mysql........."  
  if dpkg -l | grep -E '^ii' | grep mysql
    then
      _warning "Mysql is already Installed."      
      MYSQL_INSTALLED=true
    else 
      debconf-set-selections <<< 'mysql-server-5.7 mysql-server/root_password password root'
      debconf-set-selections <<< 'mysql-server-5.7 mysql-server/root_password_again password root'  
      apt-get install -y mysql-server || _die "Error while installing MySQL"
      DB_ROOT_PASS="root"
      MYSQL_INSTALLED=true
    _success "Done."
    fi

  createMysqlDbUser 
}

function installPHP()
{
  _process "Installing PHP........."  
  if dpkg -l | grep -E '^ii' | grep php7.4
  then
    _warning "PHP is already Installed"
    PHP_INSTALLED=true    
    else      
      apt -y install software-properties-common      
      add-apt-repository ppa:ondrej/php -y
      apt-get update  
      apt-get install -y php7.4 php7.4-gd php7.4-common php7.4-xml php7.4-dom || _die "Error while installing PHP"
      apt-get install -y php7.4-mysql php7.4-curl php7.4-xml php7.4-mbstring libapache2-mod-php7.4 snmp libsnmp-dev php7.4-bcmath php7.4-bz2 php7.4-intl php7.4-zip
      a2enmod rewrite
      PHP_INSTALLED=true
     _success "Done."
    fi
}

function installComposer()
{ 
  if dpkg -l | grep -E '^ii' | grep composer
  then  
  _warning "Composer is already Installed."
  COMPOSER_INSTALLED=true
  else
  _process "Downloading Composer........."  
  curl -sS https://getcomposer.org/installer | php
  composer self-update --1  
  _process "Installing Composer........."  
  apt-get install -y composer
  mv composer.phar /usr/local/bin/composer || _die "Error while installing Composer"
  COMPOSER_INSTALLED=true
  _success "Done."
  fi
}


function installDrush()
{
if [ "$COMPOSER_INSTALLED" = true ] ; then
DIR="/usr/local/lib/drush"
  if [ -d "$DIR" ]; then
    _warning "Drush is already Installed."
  DRUSH_INSTALLED=true
  else  
  cd /usr/local/lib/
    sudo rm -r drush* 
  mkdir drush
    cd drush
     _process "Installing Drush........."
  yes | composer require drush/drush:10 || _die "Error while installing Drush" 
  DRUSH_INSTALLED=true
  _success "Done."
  fi
  fi
}

function installDrupal()
{   

 if [ "$APACHE_INSTALLED" = true ] &&  [ "$MYSQL_DB_CREATED" = true ] &&  [ "$DRUSH_INSTALLED" = true ] &&  [ "$PHP_INSTALLED" = true ]; then


    cd /var/www/html
   _process "Dowloading Drupal........."
    wget https://ftp.drupal.org/files/projects/drupal-$DRUPALVERSION.tar.gz || _die "Error while Downloading Drupal"
    tar -xf drupal-$DRUPALVERSION.tar.gz 
    rm -rf drupal-$DRUPALVERSION.tar.gz 
    rm -rf $SITE_NAME 
    mv drupal-$DRUPALVERSION $SITE_NAME
    cd $SITE_NAME
    mkdir sites/default/files
    cd sites/default
    cp default.settings.php settings.php
    cp default.services.yml services.yml
    chmod 755 services.yml
    chmod 777 settings.php
    chmod 777 files

    sed -i "/\/var\/www\/html/c DocumentRoot \/var\/www\/html\/${SITE_NAME}" /etc/apache2/sites-enabled/000-default.conf        
    sed -i '/<Directory \/var\/www\/>/,/<\/Directory>/ s/AllowOverride None/AllowOverride All/' /etc/apache2/apache2.conf        
    sed -i '/max_execution_time/c max_execution_time = 3000' /etc/php/7.4/apache2/php.ini    
    service apache2 restart
    
    cd /var/www/html/$SITE_NAME

    yes | composer require --dev drush/drush
    
    #install site here
    _process "Installing Drupal........."
    COMMAND_STR="/usr/local/lib/drush/vendor/bin/drush -y si --db-url=mysql://${DB_USER}:${DB_PASS}@localhost:3306/${DB_NAME} --account-name=${SITE_USER} --account-pass=${SITE_PASS} --site-name=${SITE_NAME_RAWX}"
    echo $COMMAND_STR 

   
    eval $COMMAND_STR || _die "Error while Installing Drupal"

    chmod 755 sites/default/settings.php

    DRUPAL_INSTALLED=true
  
    _success "Done."

  fi
}


function installiitbombayx()
{

  if [ "$DRUPAL_INSTALLED" = true ]; then      

       _process "Dowloading IITBombayX profile........."  
       cd /var/www/html/$SITE_NAME
       wget http://gitlab.cse.iitb.ac.in/drupaldev/iitbombayx_public_modules_v2/-/archive/master/iitbombayx_public_modules_v2-master.tar.gz
       tar -xf iitbombayx_public_modules_v2-master.tar.gz
       cd iitbombayx_public_modules_v2-master/
       cp -dpR modules/ themes/ ../
       cp -dpR 2020-07/ ../sites/default/files/
      # cp -dpR digital_assets/ ../sites/default/files/
       rm -rf iitbombayx_public_modules_v2-master iitbombayx_public_modules_v2-master.tar.gz 
       service apache2 restart

       service mysql restart

      

        #enable theme and module here

         _success "Installing IITBombayX theme........."
         cd /var/www/html/$SITE_NAME

         COMMAND_STR="/usr/local/lib/drush/vendor/bin/drush -y then iitbombayx"
         eval $COMMAND_STR || _die "Error while enable drupal theme"

         COMMAND_STR="/usr/local/lib/drush/vendor/bin/drush -y config-set system.theme default iitbombayx"
         eval $COMMAND_STR || _die "Error while setting default drupal theme"        

         COMMAND_STR="/usr/local/lib/drush/vendor/bin/drush cr"
         eval $COMMAND_STR 

         _success "Installing IITBombayX modules.........Please wait this will take time. Ignore warnings."

         COMMAND_STR="/usr/local/lib/drush/vendor/bin/drush -y en iitbombayx_basic"
         eval $COMMAND_STR ||_die "Error while enable drupal module" 

        
         
         chmod 777 -R sites/default/files

         COMMAND_STR="/usr/local/lib/drush/vendor/bin/drush cr"
         eval $COMMAND_STR

          IITBOMBAYX_PROFILE=true
          _success "Done."
      fi
} 

function prepare()
{
  sudo apt-get install -y git tar curl
}

function loadAssets()
{
      
       cd /var/www/html/$SITE_NAME 
       FOLDERPATH="/var/www/html/${SITE_NAME}/sites/default/files/digital_assets/"
       if [ ! -d $FOLDERPATH ]; then
        _arrow "Please enter your assets directory path. (It should be Git's tar.gz path)"
        _arrow "e.g http://gitlab.cse.iitb.ac.in/drupaldev/iitbombayx_public_digital_assets/-/archive/master/iitbombayx_public_digital_assets-master.tar.gz"
        read ASSETDIRPATH

        _success "Loading Digital Assets........."
        wget $ASSETDIRPATH || loadAssetsRetry "Error downloading asset directory. Please retry."
        FILENAME=$(basename -- $ASSETDIRPATH)
        tar -xf $FILENAME
        UNZIPPEDFOLDERNAME=$(echo $FILENAME | cut -f 1 -d '.')
        mv $UNZIPPEDFOLDERNAME sites/default/files/digital_assets
       fi        

       COMMAND_STR="/usr/local/lib/drush/vendor/bin/drush en import_content"
       eval $COMMAND_STR || _die "Error while loading digital contents. Please Refer document(Section 3.2) to load assets manually."

       chmod 777 -R sites/default/files

       COMMAND_STR="/usr/local/lib/drush/vendor/bin/drush cr"
       eval $COMMAND_STR

       _success "Installation successfully completed."

       exit 0
}



function loadAssetsRetry()
{
    _error "$@"
        _arrow "Please enter your assets directory path. (It should be Git's tar.gz path)"
        _arrow "e.g http://gitlab.cse.iitb.ac.in/drupaldev/iitbombayx_public_digital_assets/-/archive/master/iitbombayx_public_digital_assets-master.tar.gz"
        read ASSETDIRPATHX
        wget $ASSETDIRPATHX || _die "Error downloading asset directory. Please Refer document(Section 3.2) to load assets manually."     
        FILENAME=$(basename -- $ASSETDIRPATHX)
        tar -xf $FILENAME      
        UNZIPPEDFOLDERNAME=$(echo $FILENAME | cut -f 1 -d '.')
        mv $UNZIPPEDFOLDERNAME sites/default/files/digital_assets
      

       COMMAND_STR="/usr/local/lib/drush/vendor/bin/drush en import_content"
       eval $COMMAND_STR || _die "Error while loading digital contents. Please Refer document(Section 3.2) to load assets manually."

       chmod 777 -R sites/default/files  

       COMMAND_STR="/usr/local/lib/drush/vendor/bin/drush cr"
       eval $COMMAND_STR

       _success "Installation successfully completed."
       exit 0
}






################################################################################
# Main
################################################################################
export LC_CTYPE=C
export LANG=C

DEBUG=0 # 1|0
_debug set -x
VERSION="0.1.0"

BIN_MYSQL=$(which mysql)

DB_HOST='localhost'
DB_PASS=$(generateSimplePassword)
SITE_NAME=
SITE_USER='admin'
SITE_PASS=$(generatePassword)


function main()
{
    _checkRootUser
    #_checkInternet
    [[ $# -lt 1 ]] && _printUsage
    _process "Processing arguments..."
    processArgs "$@"
    _success "Done!"

    _process "Preparing for installation..."
     prepare    
    _success "Done!"

    installApache
    installMysql
    installPHP
    installComposer
    installDrush
    installDrupal
    installiitbombayx
    printSuccessMessage
    loadAssets
    exit 0
}

main "$@"

_debug set +x
