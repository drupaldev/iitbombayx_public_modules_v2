<?php
/*
# This file is part of IIMBX-Drupal.
#
# IIMBX-Drupal is free software: you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free 
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# IIMBX-Drupal is distributed in the hope that it will be useful,but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
# more details.
#
# You should have received a copy of the GNU General Public License along with
# IIMBX-Drupal.  If not, see <http://www.gnu.org/licenses/>.

###############################################################################
#                                                                             #
# Purpose: It contains logic and main program of module.                      #
#                                                                             #
# Created by: Mangesh Gharate                                                 #
#                                                                             #
###############################################################################
*/
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use \Drupal\file\Entity\File;
use Drupal\Component\Serialization\Yaml;
use \Drupal\Core\Url;
use \Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\System;


   /** Implements hook_install() **/
   function import_content_install()
   {
       $folderpath = "sites/default/files/digital_assets/";
       $config_file_content = file_get_contents($folderpath.'config.yml');
       $map_array = Yaml::decode($config_file_content);
      


    ############### Logo, Favicon and Color #####################################################
    
        $theme_name = \Drupal::configFactory()->getEditable('system.theme')->get('default');

        $admin_theme = \Drupal::config('system.theme')->get('admin');

        \Drupal::configFactory()->getEditable($theme_name.'.settings')->set('logo.use_default', false) ->save();
        \Drupal::configFactory()->getEditable($theme_name.'.settings')->set('logo.path', $folderpath.$map_array['logo']) ->save();      
        \Drupal::configFactory()->getEditable($theme_name.'.settings')->set('favicon.use_default', false) ->save();
        \Drupal::configFactory()->getEditable($theme_name.'.settings')->set('favicon.path',$folderpath.$map_array['favicon']) ->save();
        \Drupal::configFactory()->getEditable($admin_theme.'.settings')->set('logo.use_default', false) ->save();
        \Drupal::configFactory()->getEditable($admin_theme.'.settings')->set('logo.path', $folderpath.$map_array['logo']) ->save();      
        \Drupal::configFactory()->getEditable($admin_theme.'.settings')->set('favicon.use_default', false) ->save();
        \Drupal::configFactory()->getEditable($admin_theme.'.settings')->set('favicon.path',$folderpath.$map_array['favicon']) ->save();     
        \Drupal::configFactory()->getEditable('color.theme.'.$theme_name)->set('palette.themecolor',$map_array['color'])->save();
        \Drupal::configFactory()->getEditable('system.theme')->set('default', $theme_name)->save();
        \Drupal::service('theme_installer')->install([$theme_name]);

    //    ################ Banner Images ##############################################################

             foreach($map_array['banners'] as $banner)
                  {
                     
                      // Create file object from a locally copied file.

                      $file1 = File::Create(['uri' => $folderpath.$banner['large'] ]);
                      $file1->save();                
                      $file2 = File::Create(['uri' => $folderpath.$banner['medium'] ]);
                      $file2->save();
                      $file3 = File::Create(['uri' => $folderpath.$banner['small'] ]);
                      $file3->save();

                      undo_banners(t($banner['title']));


                      // Create node object with attached file.
                      $node = Node::create([
                        'type' => 'home_page_',
                        'title' => t($banner['title']),
                        'status' => 1,
                        'promote' => 0,
                        'langcode' => 'en',
                        'field_link_' => $banner['link'],
                        'field_show_in_slider' => true,
                        'field_slide_image_750_x_359_' => [
                          'target_id' => $file3->id(),
                          'alt' => $banner['title'],
                          'title' => $banner['title']
                        ],
                        'field_slide_image_970_x_359_' => [
                          'target_id' => $file2->id(),
                          'alt' => $banner['title'],
                          'title' => $banner['title']
                        ],
                        'field_slide_image_1179_x_359_' => [
                          'target_id' => $file1->id(),
                          'alt' => $banner['title'],
                          'title' => $banner['title']
                        ],
                      ]);
                      $node->save();
                  }
    ################ Static Pages ###############################################################

    ####################### about ########################
                    if($map_array['static']['about'] && file_exists($folderpath.$map_array['static']['about'])){
                      $node = Node::load(1);
                      $node->body= ['value' => file_get_contents($folderpath.$map_array['static']['about']), 'format' => 'full_html'];
                      $node->save();
                    }

    ####################### faq ########################
                     if($map_array['static']['faq'] && file_exists($folderpath.$map_array['static']['faq'])){    
                      $node = Node::load(2);  
                      $node->body= ['value' => file_get_contents($folderpath.$map_array['static']['faq']), 'format' => 'full_html'];
                      $node->save();
                    }

    ####################### contact ########################
                      if($map_array['static']['contact'] && file_exists($folderpath.$map_array['static']['contact'])){
                      $node = Node::load(3);  
                      $node->body= ['value' => file_get_contents($folderpath.$map_array['static']['contact']), 'format' => 'full_html'];                    
                      $node->save();
                    }

    ####################### privacy ########################
                      if($map_array['static']['privacy'] && file_exists($folderpath.$map_array['static']['privacy'])){  
                      $node = Node::load(4);   
                      $node->body= ['value' => file_get_contents($folderpath.$map_array['static']['privacy']), 'format' => 'full_html'];
                     
                      $node->save();
                      }                   

    ####################### terms  ########################

                     if($map_array['static']['tos'] && file_exists($folderpath.$map_array['static']['tos'])){
                      $node = Node::load(5);
                      $node->body= ['value' => file_get_contents($folderpath.$map_array['static']['tos']), 'format' => 'full_html'];                 
                      $node->save();
                    }
   }




    function undo_banners($title)
   {
        $available_nids = \Drupal::entityQuery('node')->condition('type', 'home_page_')->condition('title', $title)->execute();
       
         foreach ($available_nids as $available_nid)
              {
                  $nodex = Node::load($available_nid);
                   if ($nodex) {
                       $nodex->delete();
                   }
             }
   }


      

  