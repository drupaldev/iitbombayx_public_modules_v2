/**
* This file is part of IITBombayX-Drupal.
*
* IITBombayX-Drupal is free software: you can redistribute it and/or modify it 
* under the terms of the GNU General Public License as published by the Free 
* Software Foundation, either version 3 of the License, or (at your option) any
* later version.
*
* IITBombayX-Drupal is distributed in the hope that it will be useful,but 
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
* more details.
*
* You should have received a copy of the GNU General Public License along with
* IITBombayX-Drupal.  If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************
*                                                                             *
* Purpose: This JS file provides the function for login and after login menus,*
*          search functionality and hide & display block functions.           * 
*                                                                             *
* Created by: IITBombayX-Drupal Team (Mangesh Gharate & Varun Madkaikar)     *
*                                                                             *
*******************************************************************************
*/

(function($) {
        Drupal.behaviors.myBehavior = {
        attach: function (context, settings) {
        var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
        var edxinfo= {};
        var is_loggedin = false;     
        for(var i = 0; i <ca.length; i++)
     {
          var c = ca[i];
          while (c.charAt(0) == ' ') {
           c = c.substring(1);
                }

                  if(c.indexOf(edx_cookie_name) == 0) {
                           if(c.substring(edx_cookie_name.length+1, c.length)=='true')
                            is_loggedin = true;
                        }

 
          if(c.indexOf(user_cookie_name) == 0)
          {
              var userinfo = c.substring(user_cookie_name.length+1, c.length);
//                var t1= userinfo.replace('edx-user-info=',"");
                  var t3 = userinfo.replace(/\\054/g, ',');
                  var t4 = t3.replace(/\\"/g,'"');
                  var t5= t4.substring(1,t4.length-1);
                  edxinfo = JSON.parse(t5);
                }
         }
  
        var usernamex= edxinfo['username']; 
        $("#username").html(usernamex);

        if(is_loggedin) {
            $("#auth_header").css("display", "block");
            $("#guest_header").css("display", "none");
      $("#after_login_menu").css("display", "block");
      $("#login_tab").css("display","none");
        }
        else
        {
            $("#auth_header").css("display", "none");
            $("#guest_header").css("display", "block");
            $("#after_login_menu").css("display", "none");
      $("#login_tab").css("display","block");
        }

        if(edxinfo['header_urls']){
          var profile_link = edxinfo['header_urls']['learner_profile'];
          var logout_link = edxinfo['header_urls']['logout'];
          var account_link = edxinfo['header_urls']['account_settings'];
        }
                                                                                         
        $(".profile_link").attr("href", profile_link);
        $(".logout_link").attr("href", logout_link);
        $(".account_link").attr("href", account_link);
         
       } //attach
    } //behavior

    // manage tabs underlines on front page
    $('ul#ulcolor').css('border-bottom','solid 3px #718f3e');
    $('ul#ulcolor').find('li').click(function () {
      var currentClassx = $(this).attr('class');
      var hasClasses = currentClassx.split(' ');
      var currentClass = hasClasses[0];
  if(currentClass=='teach_moocs_tab'){$('ul#ulcolor').css('border-bottom','solid 3px #e45142') }
  else if(currentClass=='life_moocs_tab'){$('ul#ulcolor').css('border-bottom','solid 3px #e4891c')}
  else if(currentClass=='skill_moocs_tab'){$('ul#ulcolor').css('border-bottom','solid 3px #438596')}
  else if(currentClass=='edu_moocs_tab'){$('ul#ulcolor').css('border-bottom','solid 3px #718f3e')}
    }); 

    //add placeholder to fulltext search course field on course search page.
    $("#edit-search-query").attr("placeholder","Course Search:");

    //code to reload youtube video on each modal close on course deatila page.
    $('#myModal').on('hidden.bs.modal', function () {
      var vpath = $("#videopathhidden").val();
      $('#ytvideo').attr('src', vpath);
    });

    //var search_key = $('#edit-search-query').val();
    var parms =	getParams(window.location.href);
    var search_key = parms['search_query'];
    //alert(search_key);
    if(search_key && search_key!='' && search_key!='undefined'){
       $("#search_key_two").val(search_key);
       document.getElementById("search_key_two").value = search_key;
    }

    var x = document.getElementsByClassName("no_result");
    var i;
     for (i = 0; i < x.length; i++) {
       x[i].innerHTML = "No courses found for keyword '"+search_key+"', Click on 'All Courses' Or Search with other keyword.";
     }

    $('.form-item-search-query').addClass('input-group');
    $('#edit-actions').addClass('input-group-btn');
    $('#edit-submit-search-course-result').addClass('custom_filter_btn');
    $('#edit-search-query').addClass('custom_filter_input');

    $(document).ajaxComplete(function( event, xhr, settings ) {
     var n = settings.url.search("/views/ajax");
     if(n!=-1)
      {
        $('.form-item-search-query').addClass('input-group');
 	$('.form-actions').addClass('input-group-btn');
	$('.form-submit').addClass('custom_filter_btn');
	$('.form-text').addClass('custom_filter_input');
        $('.form-text').attr("placeholder", "Course Search:");
      }
    });


function getParams(url) {
	var params = {};
	var parser = document.createElement('a');
	parser.href = url;
	var query = parser.search.substring(1);
	var vars = query.split('&');
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split('=');
		params[pair[0]] = decodeURIComponent(pair[1]);
	}
	return params;
}
})(jQuery);